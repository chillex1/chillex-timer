"use strict";

const applicationServerPublicKey =
    "BKnEJGDtuLg5Lf6fXGluKasFDgKpffCFXHtAjysuqpWvJ9r-Ds01-nqnqv02DpNOLECrAEgB7IDm8HeGhKtj4Ng";

// const pushButton = document.querySelector("#push-enable-btn");
// const notiButton = document.querySelector("#push-btn");
let isSubscribed = false;
let swRegistration = null;
if ("serviceWorker" in navigator && "PushManager" in window) {
    console.log("Service Worker and Push is supported");

    navigator.serviceWorker
        .register("/static/js/sw.js")
        .then(function (swReg) {
            console.log("Service Worker is registered", swReg);

            swRegistration = swReg;
            initializeUI();
            // navigator.serviceWorker.addEventListener("message", (event) => {
            //     // event is a MessageEvent object
            //     if (event.data.type == "START_WORKING") {
            //         console.log(
            //             `=====The service worker sent me a message: ${event.data}`
            //         );
            //         document.getElementById("text1").innerHTML = "yourTextHere";
            //     }
            // });
        })
        .catch(function (error) {
            console.error("Service Worker Error", error);
        });
} else {
    alert("Push messaging is not supported");
    // pushButton.textContent = "Push Not Supported";
}

function initializeUI() {
    subscribeUser(); 
    // pushButton.addEventListener("click", function () {
    //     pushButton.disabled = true;
    //     if (isSubscribed) {
    //         // TODO: Unsubscribe user
    //         unsubscribeUser();
    //     } else {
    //         subscribeUser();
    //     }
    // });
    // notiButton.addEventListener("click", function () {
    //     fetch("/api/notify", {
    //         method: "POST",
    //         body: {},
    //     });
    // });

    // Set the initial subscription value
    swRegistration.pushManager.getSubscription().then(function (subscription) {
        isSubscribed = !(subscription === null);

        updateSubscriptionOnServer(subscription);

        if (isSubscribed) {
            console.log("User IS subscribed.");
        } else {
            console.log("User is NOT subscribed.");
        }

        updateBtn();
    });
}

function subscribeUser() {
    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    swRegistration.pushManager
        .subscribe({
            userVisibleOnly: true,
            applicationServerKey: applicationServerKey,
        })
        .then(function (subscription) {
            console.log("User is subscribed.");

            updateSubscriptionOnServer(subscription);

            isSubscribed = true;

            updateBtn();
        })
        .catch(function (err) {
            console.log("Failed to subscribe the user: ", err);
            updateBtn();
        });
}

function unsubscribeUser() {
    swRegistration.pushManager
        .getSubscription()
        .then(function (subscription) {
            if (subscription) {
                // sessionStorage.removeItem("session");
                return subscription.unsubscribe();
            }
        })
        .catch(function (error) {
            console.log("Error unsubscribing", error);
        })
        .then(function () {
            updateSubscriptionOnServer(null);

            console.log("User is unsubscribed.");
            isSubscribed = false;

            //updateBtn();
        });
}

function updateBtn() {
    if (Notification.permission === "denied") {
        // pushButton.textContent = "Push Messaging Blocked.";
        // pushButton.disabled = true;
        updateSubscriptionOnServer(null);
        return;
    }

    // if (isSubscribed) {
    //     pushButton.textContent = "Disable Push Messaging";
    // } else {
    //     pushButton.textContent = "Enable Push Messaging";
    // }

    // pushButton.disabled = false;
}

function urlB64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, "+")
        .replace(/_/g, "/");

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function updateSubscriptionOnServer(subscription) {
    // TODO: Send subscription to application server

    // const subscriptionJson = document.querySelector(".js-subscription-json");
    // const subscriptionDetails = document.querySelector(
    //     ".js-subscription-details"
    // );

    if (subscription) {
        // subscriptionJson.textContent = JSON.stringify(subscription);
        // subscriptionDetails.classList.remove("is-invisible");
        fetch("/api/subscribe", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                subscription_json: JSON.stringify(subscription),
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                //console.log(data);
                //console.log(data.result.subscription_json);
                localStorage.setItem(
                    "subscription_json",
                    data.result.subscription_json
                );
            });
    } else {
        // subscriptionDetails.classList.add("is-invisible");
    }
}
