self.addEventListener("install", function (event) {
    console.log("Service Worker installing.");
});

self.addEventListener("activate", function (event) {
    console.log("Service Worker activating.");
});

self.addEventListener("push", function (event) {
    console.log("[Service Worker] Push Received.");
    console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
    const pushData = event.data.text();
    let data, title, body;
    try {
        data = JSON.parse(pushData);
        title = data.title;
        body = data.body;
    } catch (e) {
        title = "Untitled";
        body = pushData;
    }
    const options = {
        body: body,
        requireInteraction: true,
    };

    event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener("notificationclick", function (event) {
    console.log("[Service Worker] Notification click Received.");

    event.notification.close();
    self.clients
        .matchAll({
            includeUncontrolled: true,
            type: "window",
        })
        .then((clients) => {
            if (clients && clients.length) {
                console.log("send post message-------");
                // Send a response - the clients
                // array is ordered by last focused
                clients.forEach((client) =>
                    client.postMessage({
                        type: "OPEN_BREAK_TIMER",
                        count: 5,
                    })
                );
                // clients[0].postMessage({
                //     type: "REPLY_COUNT",
                //     count: 5,
                // });
            }
        });

    // event.waitUntil(clients.openWindow("/breaktime"));
});

// Listen to the request
let count;
self.addEventListener("message", (event) => {
    console.log("-----got message from post message-------");
    // console.log(event.data + " & " + event.data.type);
    if (event.data && event.data.type === "BREAK_OVER") {
        console.log("++++++got message from post message+++++");
        // Select who we want to respond to
        self.clients
            .matchAll({
                includeUncontrolled: true,
                type: "window",
            })
            .then((clients) => {
                if (clients && clients.length) {
                    // Send a response - the clients
                    // array is ordered by last focused
                    clients.forEach((client) =>
                        client.postMessage({
                            type: "START_WORKING",
                            count: 5,
                        })
                    );
                }
            });
    }
});
